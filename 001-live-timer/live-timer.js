class LiveTimer extends TimeFormatted {
  #timer = undefined;

  constructor() {
    super();
  }

  connectedCallback() {
    if (this.rendered || this.#timer) return void 0;

    this.render();
    this.#timer = setInterval(() => {
      const now = new Date();
      this.setAttribute("datetime", now);
      const on_tick = new CustomEvent("tick", { detail: now });
      this.dispatchEvent(on_tick);
    }, 1000);
    this.rendered = true;
  }

  disconnectedCallback() {
    clearInterval(this.#timer);
  }
}

customElements.define("live-timer", LiveTimer);
