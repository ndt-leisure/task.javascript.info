# My Homeworks on JavaScript.Info

> Don't worry, this "Homework" folder won't cost terrabytes of your hard disk.\
> Đăng Tú (1995 - when his girlfriend**s** found out this statement)

## What's this?

I just wanna share my solutions when I did javascript.info tasks. Feel free to
correct me with your forks and this repo issue, however no MR shall be made as
this is not open source project. It's open homework flexing!

## Task List

> **NOTE**\
> The index number of solution directories are written in hexadecimal (hex) with
> 3 digits.

1. Live timer element:
   - https://javascript.info/task/live-timer
   - [001-live-timer](./001-live-timer)
2. Task name:
   - link to source
   - [hex-index-my-solution]()
